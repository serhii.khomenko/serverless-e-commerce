import { combineReducers } from 'redux';
import productReducer from './productReducer';
import cartReducer from "./cartReducer";
import ordersReducer from "./ordersReducer";
import settingsReducer from "./settingsReducer";

export default combineReducers({
    products: productReducer,
    cart: cartReducer,
    orders: ordersReducer,
    settings: settingsReducer
});