import {API} from "aws-amplify";
import {useDispatch} from "react-redux";


export const productInitialState = {
    products: [],
    productDetails: null,

    productsIsLoading: false,
    productDetailsIsLoading: false,
    showProductDetails: false
};

export default (state = productInitialState, action) => {
    switch (action.type) {
        case 'GET_PRODUCTS':
            return {...state, productsIsLoading: true};
        case 'GET_PRODUCTS_SUCCESS':
            return {...state, ...action.data, productsIsLoading: false};
        case 'GET_PRODUCT_DETAILS':
            return {...state, productDetailsIsLoading: true, showProductDetails: true};
        case 'GET_PRODUCT_DETAILS_SUCCESS':
            let {productDetails} = action.data;

            if(productDetails.length) {
                productDetails = productDetails[0];
            }

            return {...state, productDetails, productDetailsIsLoading: false};
        case 'HIDE_PRODUCT_DETAILS':
            return {...state, showProductDetails: false};
        default:
            return state
    }
}