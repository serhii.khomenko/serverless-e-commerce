import {getProductThumbnail} from "../components/product/ProductItem";

export function calculateCartTotal(state) {
    let cartItems = state.items,
        cartTotal = cartItems.reduce((prev, curr) => {
            return (curr.price * curr.qty) + prev;
        }, 0);

    if(state.coupon !== null) {
        if(state.coupon.type === 'percentage') {
            cartTotal -= cartTotal * state.coupon.discount;
        } else {
            cartTotal -= state.coupon.discount;
        }
    }

    return cartTotal < 0 ? 0 : cartTotal;
}

export const cartInitialState = {
    items: [],
    coupon: null,
    total: 0,
    checkoutComplete: false,
    couponVerifying: false,
};

export default (state = cartInitialState, action) => {
    switch (action.type) {
        case 'CART_ADD_ITEM':
            const {productId, qty, price} = action.payload;
            let {items} = state;

            let itemIdx = items.findIndex(i => i.productId === productId);
            if(itemIdx === -1) {
                items.push(action.payload);
            } else {
                items[itemIdx].qty += qty;
            }

            return {
                ...state,
                items
            };

        case 'CART_REMOVE_ITEM':
            let cartItems = state.items.filter(i => i.productId !== action.payload.productId);
            return {...state, items: cartItems};

        case 'APPLY_COUPON':
            return {...state, couponVerifying: true};
        case 'COUPON_VERIFIED':
            return {...state, couponVerifying: false, coupon: action.data};
        case 'CART_CHECKOUT_COMPLETE':
            return {...state, checkoutComplete: true};
        default:
            return state
    }
}