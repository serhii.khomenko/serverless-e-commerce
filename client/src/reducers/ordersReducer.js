export const ordersInitialState = {
    orders: [{
        id: 10,
        status: 'processing',
        coupon: 'FREE20',
        firstName: 'Serhii',
        lastName: 'Khomenko',
        paid: 36
    }],
    showOrderDetails: false,
    currentOrderId: -1,
    orderDetails: {
        id: 10,
        items: [{
            productId: 1,
            title: 'Test',
            thumbnailUrl: '/assets/images/product-item-1.jpg',
            qty: 1,
            price: 45
        }],
        total: 45,
        coupon: 'FREE20',
        paid: 36,
        status: 'processing',
        firstName: 'Serhii',
        lastName: 'Khomenko',
        country: 'United States',
        region: 'New York',
        city: 'New York',
        zip: '61124',
        address: 'Here'
    }
};

export default (state = ordersInitialState, action) => {
    switch (action.type) {
        case 'GET_ORDERS':
            return {
                result: action.payload
            };
        case 'GET_ORDER_DETAILS':
            const {orderId} = action.payload;

            if(state.currentOrderId !== orderId) {
                // request product here
            }

            return {
                ...state,
                currentOrderId: orderId,
                showOrderDetails: true
            };
        case 'HIDE_ORDER_DETAILS':
            return {
                ...state,
                showOrderDetails: false
            };
        default:
            return state
    }
}