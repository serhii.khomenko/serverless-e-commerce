export const settingsInitialState = {
    currentScreen: 'products',
    userLoggedIn: false,
    authorisationInProcess: false,
    userUid: '',
    username: ''
};

export default (state = settingsInitialState, action) => {
    switch (action.type) {
        case 'SET_CURRENT_SCREEN':
            return {...state, currentScreen: action.screen};
        case 'LOGIN':
            return {...state, authorisationInProcess: true};
        case 'LOGIN_SUCCESS':
            console.log(action.data);
            return {
                ...state,
                authorisationInProcess: false,
                userLoggedIn: true,
                username: action.data.username,
                userUid: action.data.attributes.sub
            };
        case 'LOGOUT':
            return {...settingsInitialState};
        default:
            return state
    }
}