import React, {useEffect, useState} from 'react';

import {connect, useDispatch, useSelector} from "react-redux";
import {getProducts} from './actions/products';
import ProductItem from "./components/product/ProductItem";

function App() {
    const {products} = useSelector(state => state.products);
    const dispatch = useDispatch();

    const [productListLoaded, setProductListLoaded] = useState(false);

    useEffect(() => {
        if(!productListLoaded) {
            dispatch({type: 'GET_PRODUCTS'});
        }

        setProductListLoaded(true);
    });

    if(products === undefined) {
        return <div></div>;
    }

    return (
        <div className="main-content main-content-product no-sidebar">
            <div className="container">
                <div className="row">
                    <div className="content-area shop-grid-content full-width col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div className="site-main">
                            <h3 className="custom_blog_title">
                                Products
                            </h3>
                            <div className="shop-top-control">
                                <form className="select-item select-form">
                                    <span className="title">Sort</span>
                                    <select data-placeholder="9 Products/Page" className="chosen-select">
                                        <option value="2">9 Products/Page</option>
                                        <option value="1">12 Products/Page</option>
                                        <option value="3">10 Products/Page</option>
                                        <option value="4">8 Products/Page</option>
                                        <option value="5">6 Products/Page</option>
                                    </select>
                                </form>
                                <form className="filter-choice select-form">
                                    <span className="title">Sort by</span>
                                    <select data-placeholder="Price: Low to High" className="chosen-select">
                                        <option value="1">Price: Low to High</option>
                                        <option value="2">Sort by popularity</option>
                                        <option value="3">Sort by average rating</option>
                                        <option value="4">Sort by newness</option>
                                        <option value="5">Sort by price: low to high</option>
                                    </select>
                                </form>
                            </div>
                            <ul className="row list-products auto-clear equal-container product-grid">

                                { products.map(item => <ProductItem product={item} key={item.id} />) }

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default connect(
    state => ({...state}),
    dispatch => ({
        getProducts: () => dispatch(getProducts())
    })
)(App);
