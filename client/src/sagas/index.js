import { all, call, delay, put, takeEvery } from 'redux-saga/effects'
import {API, Auth} from "aws-amplify";

function* getProducts(params) {
    const response = yield call(() => API.get('remote', '/products'), params);
    yield put({
        type: 'GET_PRODUCTS_SUCCESS',
        data: response
    });
}

function* getProductDetails(params) {
    const response = yield call(() => API.get('remote', `/products/${params.id}`), params);
    yield put({
        type: 'GET_PRODUCT_DETAILS_SUCCESS',
        data: response
    });
}

function* verifyCoupon(params) {
    const response = yield call(() => API.get('remote', `/coupon/${params.code}`), params);
    yield put({
        type: 'COUPON_VERIFIED',
        data: response
    });
}

function* login(params) {
    const response = yield call(() => Auth.signIn(params.email, params.password));
    yield put({
        type: 'LOGIN_SUCCESS',
        data: response
    });
}


function* productSaga() {
    yield takeEvery('GET_PRODUCTS', getProducts);
    yield takeEvery('GET_PRODUCT_DETAILS', getProductDetails);
}

function* cartSaga() {
    yield takeEvery('APPLY_COUPON', verifyCoupon);
}

function* userSaga() {
    yield takeEvery('LOGIN', login);
}

export default function* rootSaga() {
    yield all([
        call(productSaga),
        call(cartSaga),
        call(userSaga)
    ])
}