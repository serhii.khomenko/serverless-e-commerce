import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {getProductRatingStars, getProductThumbnail} from "./ProductItem";
import Text from "./Attributes/Text";
import Color from "./Attributes/Color";

const ProductDetails = () => {
    const {showProductDetails, productDetailsIsLoading, productDetails} = useSelector(state => state.products);
    const dispatch = useDispatch();
    const [qty, setQty] = useState(1);

    if(productDetails === undefined || productDetails === null) {
        return <div></div>;
    }

    const getProductAttribute = attr => {
        const {type} = attr;

        switch (type) {
            case 'color':
                return <Color />;
            case 'text':
                return <Text label={attr.label} options={attr.options} />
        }
    };

    const updateQty = inc => {
        if(qty + inc > 0)
            setQty(qty + inc)
    };

    const addToCart = () => {
        dispatch({
            type: 'CART_ADD_ITEM',
            payload: {
                productId: productDetails._id,
                title: productDetails.title,
                thumbnailUrl: getProductThumbnail(productDetails),
                qty,
                price: productDetails.price
            }
        });

        dispatch({type: 'HIDE_PRODUCT_DETAILS'})
    };

    return (
        <div style={{display: (showProductDetails && !productDetailsIsLoading) ? 'block' : 'none'}}>
            <div className="mfp-bg mfp-ready"></div>
            <div className="mfp-wrap mfp-close-btn-in mfp-auto-cursor mfp-ready" style={{overflow: 'hidden auto'}}>
                <div className="mfp-container mfp-s-ready mfp-inline-holder">
                    <div className="mfp-content">
                        <div className="kt-popup-quickview ">
                            <div className="details-thumb">
                                {productDetails.thumbnails.map(
                                    thumb => (<div className="details-item"><img src={thumb} alt="thumbnail"/></div>)
                                )}
                            </div>
                            <div className="details-infor">
                                <h1 className="product-title">{ productDetails.title }</h1>

                                {getProductRatingStars(productDetails)}

                                <div className="availability">Availability: <a>{productDetails.available}</a></div>
                                <div className="price"><span>€{productDetails.price}</span></div>
                                <div className="product-details-description"
                                     dangerouslySetInnerHTML={{__html: productDetails.description}}></div>
                                <div className="variations">

                                    {productDetails.attributes.map(a => getProductAttribute(a))}

                                </div>
                                <div className="group-button">
                                    <div className="quantity-add-to-cart">
                                        <div className="quantity">
                                            <div className="control">
                                                <span className="btn-number qtyminus quantity-minus"
                                                      onClick={() => updateQty(-1)}>-</span>
                                                <input type="text" data-step="1" data-min="0" value={qty}
                                                       className="input-qty qty" size="4"/>
                                                <span className="btn-number qtyplus quantity-plus"
                                                      onClick={() => updateQty(1)}>+</span>
                                            </div>
                                        </div>
                                        <button className="single_add_to_cart_button button" onClick={addToCart}>Add to cart</button>
                                    </div>
                                </div>
                            </div>
                            <button type="button" className="mfp-close" onClick={() => dispatch({type: 'HIDE_PRODUCT_DETAILS'})}>×</button>
                        </div>
                    </div>
                    <div className="mfp-preloader">Loading...</div>
                </div>
            </div>
        </div>
    );
};

export default ProductDetails