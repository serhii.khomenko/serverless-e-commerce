import React from 'react';
import {useDispatch} from "react-redux";

export const getProductThumbnail = product => {
    if(product.thumbnails.length) {
        return product.thumbnails[0]
    }

    return '/logo512.png'
};

export const getProductRatingStars = (product) => {
    let {rate, stars} = product,
        drawStars = Math.floor(rate + 0.25);

    return <div className="stars-rating">
        <div className="star-rating">
            {!!drawStars && <span className={`star-${drawStars}`}></span>}
        </div>
        <div className="count-star">({stars})</div>
    </div>
};

const ProductItem = ({product}) => {
    const {title, price, reviews} = product;
    const dispatch = useDispatch();

    const showQuickView = () => {
        console.log(product);
        dispatch({type: 'GET_PRODUCT_DETAILS', id: product._id})
    };

    return (
        <li className="product-item  col-lg-3 col-md-4 col-sm-6 col-xs-6 col-ts-12 style-1">
            <div className="product-inner equal-element">
                <div className="product-top">
                    <div className="flash">
                        <span className="onnew">
                            <span className="text">new</span>
                        </span>
                    </div>
                </div>
                <div className="product-thumb">
                    <div className="thumb-inner">
                        <img src={getProductThumbnail(product)} alt={title} />
                    </div>
                    <span className="button quick-wiew-button" onClick={showQuickView}>Quick View</span>
                </div>
                <div className="product-info">
                    <h5 className="product-name product_title">{title}</h5>
                    <div className="group-info">

                        {getProductRatingStars(product)}

                        <div className="price">€{price}</div>
                    </div>
                </div>
                <div className="loop-form-add-to-cart">
                    <button className="single_add_to_cart_button button" onClick={showQuickView}>Details</button>
                </div>
            </div>
        </li>
    );
};

export default ProductItem;
