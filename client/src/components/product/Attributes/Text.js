import React from 'react';
import {useDispatch} from "react-redux";

const Text = ({label, options}) => {
    const dispatch = useDispatch();

    return (
        <div className="attribute attribute_size">
            <div className="size-text text-attribute">{label}:</div>
            <div className="list-size list-item">
                {options.map(o => <a href="#">{o}</a>)}
            </div>
        </div>
    );
};

export default Text