import React from 'react';
import {useDispatch} from "react-redux";

const Color = () => {
    const dispatch = useDispatch();

    return (
        <div className="attribute attribute_color">
            <div className="color-text text-attribute">
                Color:
                <span>White/</span>
                <span>Black/</span>
                <span>Teal/</span>
                <span>Brown</span>
            </div>
            <div className="list-color list-item">
                <a href="#" className="color1"></a>
                <a href="#" className="color2"></a>
                <a href="#" className="color3 active"></a>
                <a href="#" className="color4"></a>
            </div>
        </div>
    );
};

export default Color