import React, {useState} from "react";
import {connect, useDispatch, useSelector} from "react-redux";
import MiniCart from "./MiniCart";
import setCurrentScreen from "../../actions/setCurrentScreen";
import {API} from "aws-amplify";

const Header = () => {
    const cart = useSelector(state => state.cart);
    const settings = useSelector(state => state.settings);
    const dispatch = useDispatch();

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const login = () => {
        dispatch({type: 'LOGIN', email, password});
        setEmail('');
        setPassword('');
    };

    const logout = () => dispatch({type: 'LOGOUT'});

    const signUp = () => {
        API.get('remote', '/checkout').then(res => console.log(res));
    };

    const getAccountBlock = () => {
        return (<div className="block-account block-header vereesa-dropdown">
            <a href="#" data-vereesa="vereesa-dropdown">
                <i className="fa fa-user-o" aria-hidden="true"></i>
            </a>
            <div className="header-account vereesa-submenu">
                <div className="header-user-form-tabs">
                    <ul className="tab-link">
                        <li className="active">
                            <a data-toggle="tab" aria-expanded="true"
                               href="#header-tab-login">Login</a>
                        </li>
                        <li>
                            <a data-toggle="tab" aria-expanded="true"
                               href="#header-tab-rigister">Register</a>
                        </li>
                    </ul>
                    <div className="tab-container">
                        <div id="header-tab-login" className="tab-panel active">
                            {getLoginForm()}
                        </div>
                        <div id="header-tab-rigister" className="tab-panel">
                            {getRegisterForm()}
                        </div>
                    </div>
                </div>
            </div>
        </div>)
    };

    const getLoginForm = () => {
        return (<div className="login form-login">
            <p className="form-row form-row-wide">
                <input type="email" placeholder="Email" className="input-text"
                       value={email} onChange={e => setEmail(e.target.value)} />
            </p>
            <p className="form-row form-row-wide">
                <input type="password" className="input-text" placeholder="Password"
                       value={password} onChange={e => setPassword(e.target.value)} />
            </p>
            <p className="form-row">
                <input type="submit" className="button" value="Login" onClick={login} />
            </p>
        </div>);
    };

    const getRegisterForm = () => {
        return (<div className="register form-register">
            <p className="form-row form-row-wide">
                <input type="email" placeholder="Email" className="input-text"
                       value={email} onChange={e => setEmail(e.target.value)} />
            </p>
            <p className="form-row form-row-wide">
                <input type="password" className="input-text" placeholder="Password"
                       value={password} onChange={e => setPassword(e.target.value)} />
            </p>
            <p className="form-row">
                <input type="submit" className="button" value="Register" onClick={signUp} />
            </p>
        </div>);
    };

    return (
        <header className="header style7">
            <div className="container">
                <div className="main-header">
                    <div className="row">
                        <div className="col-lg-3 col-sm-4 col-md-3 col-xs-7 col-ts-12 header-element">
                            <div className="logo" onClick={setCurrentScreen('products')}>
                                <img src="/assets/images/logo.png" alt="img"/>
                            </div>
                        </div>
                        <div className="col-lg-7 col-sm-8 col-md-6 col-xs-5 col-ts-12"></div>
                        <div className="col-lg-2 col-sm-12 col-md-3 col-xs-12 col-ts-12">
                            <div className="header-control">
                                <div className="block-minicart vereesa-mini-cart block-header vereesa-dropdown">
                                    <a href="#" className="shopcart-icon" data-vereesa="vereesa-dropdown">
                                        Cart <span className="count">{cart.items.length}</span>
                                    </a>

                                    <MiniCart />
                                </div>

                                {settings.userLoggedIn && <div className="block-account block-header">
                                    <a href="#" onClick={logout}>
                                        <i className="fa fa-user-o" aria-hidden="true"></i>
                                    </a>
                                </div>}

                                {!settings.userLoggedIn && getAccountBlock()}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
);
};

export default connect(
    state => ({products: state.products}),
    dispatch => ({})
)(Header);