import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import setCurrentScreen from "../../actions/setCurrentScreen";
import {calculateCartTotal} from "../../reducers/cartReducer";

const MiniCart = () => {
    const cart = useSelector(st => st.cart);
    const dispatch = useDispatch();

    return (
        <div className="shopcart-description vereesa-submenu">
            <div className="content-wrap">
                <h3 className="title">Shopping Cart</h3>
                <ul className="minicart-items">

                    {cart.items.map((cartItem, idx) => (
                        <li className="product-cart mini_cart_item" key={idx}>
                            <div className="product-media">
                                <img src={cartItem.thumbnailUrl} />
                            </div>
                            <div className="product-details">
                                <h5 className="product-name">{cartItem.title}</h5>
                                <div className="variations hidden">
                                    <span className="attribute_color">Black</span>,
                                    <span className="attribute_size">xl</span>
                                </div>
                                <span className="product-price">
                                    <span className="price">
                                        <span>&euro; {cartItem.price}</span>
                                    </span>
                                </span>
                                <span className="product-quantity"> x{cartItem.qty}</span>
                                <div className="product-remove" onClick={() => dispatch(
                                    {type: 'CART_REMOVE_ITEM', payload: {productId: cartItem.productId}}
                                )}>
                                    <i className="fa fa-trash-o" aria-hidden="true"></i>
                                </div>
                            </div>
                        </li>
                    ))}

                </ul>
                <div className="subtotal">
                    <span className="total-title">Subtotal: </span>
                    <span className="total-price">
                        <span className="Price-amount">
                            €{calculateCartTotal(cart)}
                        </span>
                    </span>
                </div>
                <div className="actions">
                    <div className="button button-viewcart" onClick={setCurrentScreen('cart')}>
                        <span>View Bag</span>
                    </div>
                    <div className="button button-checkout" onClick={setCurrentScreen('checkout')}>
                        <span>Checkout</span>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default MiniCart