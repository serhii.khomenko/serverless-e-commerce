import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {calculateCartTotal} from "../../reducers/cartReducer";
import setCurrentScreen from "../../actions/setCurrentScreen";

function Cart() {
    const cart = useSelector(state => state.cart);
    const {items} = cart;
    const dispatch = useDispatch();

    const [coupon, setCoupon] = useState('');
    const applyCouponCode = () => {
        dispatch({type: 'APPLY_COUPON', code: coupon});
        setCoupon('');
    };

    return (
        <div className="main-content main-content-product no-sidebar">
            <div className="container">
                <div className="row">
                    <div className="main-content-cart main-content col-sm-12">
                        <h3 className="custom_blog_title">
                            Shopping Cart
                        </h3>
                        <div className="page-main-content">
                            <div className="shoppingcart-content">
                                <form className="cart-form">
                                    <table className="shop_table">
                                        <thead>
                                        <tr>
                                            <th className="product-remove"></th>
                                            <th className="product-thumbnail"></th>
                                            <th className="product-name"></th>
                                            <th className="product-price"></th>
                                            <th className="product-quantity"></th>
                                            <th className="product-subtotal"></th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        {items.map((cartItem, idx) => (
                                            <tr className="cart_item" key={idx}>
                                                <td className="product-remove">
                                                    <a className="remove" onClick={() => dispatch({
                                                        type: 'CART_REMOVE_ITEM',
                                                        payload: {productId: cartItem.productId}
                                                    })}>&nbsp;</a>
                                                </td>
                                                <td className="product-thumbnail">
                                                    <img src={cartItem.thumbnailUrl} alt="img" className="wp-post-image"/>
                                                </td>
                                                <td className="product-name" data-title="Product">
                                                    <p className="title">{cartItem.title}</p>
                                                    <span className="attributes-select attributes-color">White,</span>
                                                    <span className="attributes-select attributes-size">M</span>
                                                </td>
                                                <td className="product-quantity" data-title="Quantity">
                                                    <div className="quantity">
                                                        <div className="control">
                                                            <input type="text" value={cartItem.qty} disabled
                                                                   className="input-qty qty" size="4"/>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td className="product-price" data-title="Price">
													<span className="woocommerce-Price-amount amount">
														<span className="woocommerce-Price-currencySymbol">
															€
														</span>
														{cartItem.price * cartItem.qty}
													</span>
                                                </td>
                                            </tr>
                                        ))}

                                        <tr>
                                            <td className="actions">
                                                <div className="coupon">
                                                    <label className="coupon_code">Coupon Code:</label>
                                                    <input type="text" className="input-text" value={coupon}
                                                           onChange={e => setCoupon(e.target.value)}
                                                           placeholder="Promotion code here"/>
                                                    <a className="button" onClick={applyCouponCode}></a>
                                                </div>
                                                <div className="order-total">
                                                    <span className="title">
                                                        Total Price:
                                                    </span>
                                                    <span className="total-price">
															€{calculateCartTotal(cart)}
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </form>
                                <div className="control-cart">
                                    <button className="button btn-continue-shopping" onClick={setCurrentScreen('products')}>
                                        CONTINUE SHOPPING
                                    </button>
                                    <button className="button btn-cart-to-checkout" onClick={setCurrentScreen('checkout')}>
                                        CHECK OUT
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Cart;