import React from 'react';
import {useDispatch, useSelector} from "react-redux";

function OrdersDetails() {
    const {orderDetails} = useSelector(state => state.orders);
    const {items, coupon, firstName, lastName, country, region, city, address, zip, paid} = orderDetails;
    const dispatch = useDispatch();

    const hideDetails = () => {
        dispatch({type: 'HIDE_ORDER_DETAILS'})
    };

    return (
        <div className="shipping-address-form-wrapp">
            <div className="shipping-address-form  checkout-form">
                <div className="row-col-1 row-col">
                    <div className="shipping-address">
                        <h3 className="title-form">
                            Your Order
                        </h3>
                        <ul className="list-product-order">
                            {items.map((cartItem, idx) => (
                                <li className="product-item-order" key={idx}>
                                    <div className="product-thumb">
                                        <img src={cartItem.thumbnailUrl} alt="img"/>
                                    </div>
                                    <div className="product-order-inner">
                                        <h5 className="product-name">
                                            {cartItem.title}
                                        </h5>
                                        <span className="attributes-select attributes-color">Black,</span>
                                        <span className="attributes-select attributes-size">XXL</span>
                                        <div className="price">
                                            €{cartItem.price}
                                            <span className="count">
                                                        x {cartItem.qty} <br/>
                                                    Subtotal: €{cartItem.price * cartItem.qty}
                                                    </span>
                                        </div>
                                    </div>
                                </li>
                            ))}
                        </ul>
                        <div className="order-total">
                            {coupon.length > 0 && <><strong>Coupon: </strong> <i>{coupon}</i> <br /></>}
                            <span className="title">Total Price: </span>
                            <span className="total-price">€{paid}</span>
                        </div>
                    </div>
                </div>

                <div className="row-col-2 row-col">
                    <div className="your-order">
                        <h3 className="title-form">
                            Shipping Address
                        </h3>

                        <p className="form-row form-row-first">
                            <strong>First name: </strong>{firstName}
                        </p>

                        <p className="form-row form-row-first">
                            <strong>Last name: </strong>{lastName}
                        </p>

                        <p className="form-row form-row-first">
                            <strong>Country: </strong>{country}
                        </p>

                        <p className="form-row form-row-first">
                            <strong>State: </strong>{region}
                        </p>

                        <p className="form-row form-row-first">
                            <strong>City: </strong>{city}
                        </p>

                        <p className="form-row form-row-first">
                            <strong>ZIP code: </strong>{zip}
                        </p>

                        <p className="form-row form-row-first">
                            <strong>Address: </strong>{address}
                        </p>
                    </div>
                </div>
            </div>
            <a className="button button-payment" onClick={hideDetails}>Hide details</a>
        </div>
    );
}

export default OrdersDetails;