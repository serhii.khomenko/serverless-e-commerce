import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {calculateCartTotal} from "../../reducers/cartReducer";

function Checkout() {
    const cart = useSelector(state => state.cart);
    const {items, coupon, checkoutComplete} = cart;
    const dispatch = useDispatch();

    const [country, setCountry] = useState('');
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [region, setRegion] = useState('');
    const [city, setCity] = useState('');
    const [address, setAddress] = useState('');
    const [zip, setZip] = useState('');

    const getPriceToBePaid = price => {
        switch (coupon.type) {
            case 'percentage':
                price -= price * coupon.discount;
                break;
            default:
                price -= coupon.discount;
        }

        return price
    };

    const applyCouponCode = () => {

    };

    const completeCheckout = () => {
        dispatch({type: 'CART_CHECKOUT_COMPLETE'})
    };

    const getConfirmationForm = () => {
        return <div className="end-checkout-wrapp">
            <div className="end-checkout checkout-form">
                <div className="icon">
                </div>
                <h3 className="title-checkend">
                    Congratulation! Your order has been processed.
                </h3>
                <div className="sub-title">
                    Aenean dui mi, tempus non volutpat eget, molestie a orci.
                    Nullam eget sem et eros laoreet rutrum.
                    Quisque sem ante, feugiat quis lorem in.
                </div>
                <a href="/" className="button btn-return">Return to Store</a>
            </div>
        </div>
    };

    const getShippingForm = () => {
        return <div className="shipping-address-form-wrapp">
            <div className="shipping-address-form  checkout-form">
                <div className="row-col-1 row-col">
                    <div className="shipping-address">
                        <h3 className="title-form">
                            Shipping Address
                        </h3>

                        <p className="form-row form-row-first">
                            <label className="text">First name</label>
                            <input type="text" className="input-text" value={firstName}
                                   onChange={e => setFirstName(e.target.value)}/>
                        </p>

                        <p className="form-row form-row-last">
                            <label className="text">Last name</label>
                            <input type="text" className="input-text" value={lastName}
                                   onChange={e => setLastName(e.target.value)}/>
                        </p>

                        <p className="form-row forn-row-col forn-row-col-1">
                            <label className="text">Country</label>
                            <select data-placeholder="United Kingdom" className="chosen-select"
                                    onChange={e => setCountry(e.target.value)} value={country}>
                                <option value="United States">United States</option>
                                <option value="United Kingdom">United Kingdom</option>
                                <option value="Afghanistan">Afghanistan</option>
                                <option value="Aland Islands">Aland Islands</option>
                                <option value="Albania">Albania</option>
                                <option value="Algeria">Algeria</option>
                                <option value="American Samoa">American Samoa</option>
                                <option value="Andorra">Andorra</option>
                                <option value="Angola">Angola</option>
                                <option value="Anguilla">Anguilla</option>
                                <option value="Antarctica">Antarctica</option>
                                <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                <option value="Argentina">Argentina</option>
                                <option value="Armenia">Armenia</option>
                                <option value="Aruba">Aruba</option>
                                <option value="Australia">Australia</option>
                                <option value="Austria">Austria</option>
                                <option value="Azerbaijan">Azerbaijan</option>
                                <option value="Bahamas">Bahamas</option>
                                <option value="Bahrain">Bahrain</option>
                                <option value="Bangladesh">Bangladesh</option>
                                <option value="Barbados">Barbados</option>
                                <option value="Belarus">Belarus</option>
                                <option value="Belgium">Belgium</option>
                                <option value="Belize">Belize</option>
                                <option value="Benin">Benin</option>
                                <option value="Bermuda">Bermuda</option>
                                <option value="Bhutan">Bhutan</option>
                            </select>
                        </p>

                        <p className="form-row forn-row-col forn-row-col-2">
                            <label className="text">State</label>
                            <select data-placeholder="London" className="chosen-select"
                                    onChange={e => setRegion(e.target.value)} value={region}>
                                <option value="United States">London</option>
                                <option value="United Kingdom">tokyo</option>
                                <option value="Afghanistan">Seoul</option>
                                <option value="Aland Islands">Mexico city</option>
                                <option value="Albania">Mumbai</option>
                                <option value="Algeria">Delhi</option>
                                <option value="American Samoa">New York</option>
                                <option value="Andorra">Jakarta</option>
                                <option value="Angola">Sao Paulo</option>
                                <option value="Anguilla">Osaka</option>
                                <option value="Antarctica">Karachi</option>
                                <option value="Antigua and Barbuda">Matx-cơ-va</option>
                                <option value="Argentina">Toronto</option>
                                <option value="Armenia">Boston</option>
                            </select>
                        </p>

                        <p className="form-row forn-row-col forn-row-col-3">
                            <label className="text">City</label>
                            <select data-placeholder="London" className="chosen-select"
                                    onChange={e => setCity(e.target.value)} value={city}>
                                <option value="United States">London</option>
                                <option value="United Kingdom">tokyo</option>
                                <option value="Afghanistan">Seoul</option>
                                <option value="Aland Islands">Mexico city</option>
                                <option value="Albania">Mumbai</option>
                                <option value="Algeria">Delhi</option>
                                <option value="American Samoa">New York</option>
                                <option value="Andorra">Jakarta</option>
                                <option value="Angola">Sao Paulo</option>
                                <option value="Anguilla">Osaka</option>
                                <option value="Antarctica">Karachi</option>
                                <option value="Antigua and Barbuda">Matx-cơ-va</option>
                                <option value="Argentina">Toronto</option>
                                <option value="Armenia">Boston</option>
                            </select>
                        </p>

                        <p className="form-row form-row-first">
                            <label className="text">Zip code</label>
                            <input type="text" className="input-text" value={zip}
                                   onChange={e => setZip(e.target.value)}/>
                        </p>

                        <p className="form-row form-row-last">
                            <label className="text">Address</label>
                            <input type="text" className="input-text" value={address}
                                   onChange={e => setAddress(e.target.value)}/>
                        </p>
                    </div>
                </div>

                <div className="row-col-2 row-col">
                    <div className="your-order">
                        <h3 className="title-form">
                            Your Order
                        </h3>
                        <ul className="list-product-order">
                            {items.map((cartItem, idx) => (
                                <li className="product-item-order" key={idx}>
                                    <div className="product-thumb">
                                        <img src={cartItem.thumbnailUrl} alt="img"/>
                                    </div>
                                    <div className="product-order-inner">
                                        <h5 className="product-name">
                                            {cartItem.title}
                                        </h5>
                                        <span className="attributes-select attributes-color">Black,</span>
                                        <span className="attributes-select attributes-size">XXL</span>
                                        <div className="price">
                                            €{cartItem.price}
                                            <span className="count">
                                                        x {cartItem.qty} <br/>
                                                    Subtotal: €{cartItem.price * cartItem.qty}
                                                    </span>
                                        </div>
                                    </div>
                                </li>
                            ))}
                        </ul>
                        <div className="order-total">
                            <span className="title">
										Total Price:
                            </span>
                            <span className="total-price">
                                €{calculateCartTotal(cart)}
                            </span>
                    </div>
                    </div>
                </div>
            </div>
            <a href="#" className="button button-payment" onClick={completeCheckout}>PAYMENT</a>
        </div>
    };

    return (
        <div className="main-content main-content-checkout">
            <div className="container">
                <h3 className="custom_blog_title">
                    Checkout
                </h3>
                <div className="checkout-wrapp">

                    {!checkoutComplete && getShippingForm()}
                    {checkoutComplete && getConfirmationForm()}

                </div>
            </div>
        </div>
    );
}

export default Checkout;