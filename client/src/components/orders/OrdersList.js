import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import OrdersDetails from "./OrderDetails";

function OrdersList() {
    const {orders, showOrderDetails, currentOrderId} = useSelector(state => state.orders);
    const dispatch = useDispatch();

    const getOrderDetails = oid => {
        dispatch({type: 'GET_ORDER_DETAILS', payload: {orderId: oid}});
    };

    const getOrder = (order, idx) => {
        return (<div className="shipping-address-form-wrapp" key={idx}>
            <div className="shipping-address-form  checkout-form">
                <div className="row-col-1 row-col">
                    <div className="shipping-address">
                        <h3 className="title-form">
                            Order #{order.id}
                        </h3>

                        <p className="form-row form-row-first">
                            <strong>Owner: </strong>{order.firstName} {order.lastName}
                        </p>

                        <p className="form-row form-row-first">
                            <strong>Status: </strong>{order.status}
                        </p>

                        <div className="order-total">
                            {order.coupon.length > 0 && <><strong>Coupon: </strong> <i>{order.coupon}</i> <br /></>}
                            <span className="title">Total Price: </span>
                            <span className="total-price">€{order.paid}</span>
                        </div>
                    </div>

                    <a className="button button-payment" onClick={() => getOrderDetails(order.id)}>Details</a>
                </div>
            </div>
        </div>);
    };

    return (
        <div className="main-content main-content-checkout">
            <div className="container">
                <h3 className="custom_blog_title">
                    My Orders
                </h3>
                <div className="checkout-wrapp">

                    { orders.map((order, idx) => {
                        return (<>
                            {getOrder(order, idx)}
                            {showOrderDetails && currentOrderId === order.id && <OrdersDetails />}
                        </>);
                    }) }

                </div>
            </div>
        </div>
    );
}

export default OrdersList;