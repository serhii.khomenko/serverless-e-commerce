import {useDispatch} from "react-redux";

export default function(screen) {
    const dispatch = useDispatch();
    return () => dispatch({type: 'SET_CURRENT_SCREEN', screen})
}