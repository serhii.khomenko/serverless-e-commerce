import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';

import {Provider} from 'react-redux';
import configureStore from "./store";

import Amplify, {API, Auth} from 'aws-amplify';
import {
    apiGatewayRegion,
    apiGatewayUrl,
    cognitoRegion,
    cognitoUserPool,
    cognitoAppClient,
    cognitoIdentity
} from "./config";
import Header from "./components/common/Header";
import ProductDetails from "./components/product/ProductDetails";
import OrderDetails from "./components/orders/OrderDetails";
import Cart from "./components/orders/Cart";
import Checkout from "./components/orders/Checkout";
import OrdersList from "./components/orders/OrdersList";

Amplify.configure({
    Auth: {
        mandatorySignIn: true,
        region: cognitoRegion,
        userPoolId: cognitoUserPool,
        identityPoolId: cognitoIdentity,
        userPoolWebClientId: cognitoAppClient
    },
    API: {
        endpoints: [
            {
                name: 'remote',
                endpoint: apiGatewayUrl,
                region: apiGatewayRegion
            }
        ]
    }
});

const store = configureStore();

function mainRender() {
    const {settings} = store.getState();

    const getRootComponent = () => {
        switch (settings.currentScreen) {
            case "cart":
                return <Cart/>
            case "checkout":
                return <Checkout/>
            default:
                return <App/>
        }
    };

    ReactDOM.render(
        <React.StrictMode>
            <Provider store={store}>
                <ProductDetails />

                <Header />

                {getRootComponent()}
            </Provider>
        </React.StrictMode>,
        document.getElementById('root')
    );
}

mainRender();
store.subscribe(mainRender);

// Auth.signIn('khomenko.mail@gmail.com', 'D3veloper!').then(u => console.log(u));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
