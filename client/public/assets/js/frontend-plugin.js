jQuery(document).ready(function ($) {
    "use strict";

    function vereesa_init_menu_toggle() {
        var contain = '.vereesa-nav-toggle';
        $(contain).each(function () {
            var _main = $(this);
            _main.children('.menu-item.parent').each(function () {
                var curent = $(this).find('.submenu');

                $(this).children('.toggle-submenu').on('click', function () {
                    $(this).parent().children('.submenu').slideToggle(500);
                    _main.find('.submenu').not(curent).slideUp(500);

                    $(this).parent().toggleClass('show-submenu');
                    _main.find('.menu-item.parent').not($(this).parent()).removeClass('show-submenu');
                });

                var next_curent = $(this).find('.submenu');

                next_curent.children('.menu-item.parent').each(function () {

                    var child_curent = $(this).find('.submenu');
                    $(this).children('.toggle-submenu').on('click', function () {
                        $(this).parent().parent().find('.submenu').not(child_curent).slideUp(500);
                        $(this).parent().children('.submenu').slideToggle(500);

                        $(this).parent().parent().find('.menu-item.parent').not($(this).parent()).removeClass('show-submenu');
                        $(this).parent().toggleClass('show-submenu');
                    })
                });
            });
        });
    };
    // click menu
    $(document).on('click', '.bar-open-menu', function () {
        $(this).toggleClass('active');
        $(this).closest('.main-header').find('.header-nav').toggleClass('show-menu');
        return false;
    });
    // vertical-menu
    $(document).on('click', '.block-title', function () {
        $(this).closest('.block-nav-categori').toggleClass('active');
        $(this).closest('.block-nav-categori').find('.verticalmenu-content').toggleClass('show-up');
        return false;
    });
    $(document).on('click', '.bar-open-menu,.vertical-menu-overlay', function () {
        $('body').toggleClass('vertical-menu-open');
        return false;
    })
    $(document).on('click', '.error-404 .toggle-hightlight', function () {
        $(this).closest('.text-404').find('.search-form').toggleClass('open');
        return false;
    });
    // ----------vereesa_custom_scrollbar-------------------
    function vereesa_custom_scrollbar() {
        $('.vereesa-mini-cart .minicart-items').mCustomScrollbar();
        $('.vereesa-mini-cart .minicart-items').change(function () {
            $('.vereesa-mini-cart .minicart-items').mCustomScrollbar();
        });
    }

    function vereesa_custom_scrollbar_header_nav() {
        $('.header.vertical-style .header-nav .container-wapper').mCustomScrollbar();
        $('.header.vertical-style .header-nav .container-wapper').change(function () {
            $('.header.vertical-style .header-nav .container-wapper').mCustomScrollbar();
        });
    }

    // --------------------remove_class_equal--------------------------
    function vereesa_remove_class_review() {
        var _winw = $(window).innerWidth();
        if (_winw < 992) {
            $('.sevice-item.style-1').removeClass('equal-container').find('.equal-element').removeAttr('style');
        }
        else {
            $('.sevice-item.style-1').addClass('equal-container');
        }
    }

    // -----------vereesa_details_thumd--------------------
    function vereesa_details_thumd_zoom() {
        /* ------------------------------------------------
         Arctic modal
         ------------------------------------------------ */
        if ($.arcticmodal) {
            $.arcticmodal('setDefault', {
                type: 'ajax',
                ajax: {
                    cache: false
                },
                afterOpen: function (obj) {

                    var mw = $('.modal_window');

                    mw.find('.custom_select').customSelect();

                    mw.find('.product_preview .owl_carousel').owlCarousel({
                        margin: 10,
                        themeClass: 'thumbnails_carousel',
                        nav: true,
                        navText: [],
                        rtl: window.ISRTL ? true : false
                    });

                    Core.events.productPreview();

                    addthis.toolbox('.addthis_toolbox');
                }
            });
        }
        // ---------Popup sizechart---------------
        if ($('#size_chart').length > 0) {
            $('#size_chart').fancybox();
        }

        if ($.fancybox) {
            $.fancybox.defaults.direction = {
                next: 'left',
                prev: 'right'
            }
        }
        /* ------------------------------------------------
         Fancybox
         ------------------------------------------------ */
        if ($('.fancybox_item').length) {
            $('.fancybox_item').fancybox({
                openEffect: 'elastic',
                closeEffect: 'elastic',
                helpers: {
                    overlay: {
                        css: {
                            'background': 'rgba(0,0,0, .6)'
                        }
                    },
                    thumbs: {
                        width: 50,
                        height: 50
                    }
                }
            });
        }
        if ($('.fancybox_item_media').length) {
            $('.fancybox_item_media').fancybox({
                openEffect: 'none',
                closeEffect: 'none',
                helpers: {
                    media: {}
                }
            });
        }
        /* ------------------------------------------------
         Elevate Zoom
         ------------------------------------------------ */
        if ($('#img_zoom').length) {
            $('#img_zoom').elevateZoom({
                zoomType: "inner",
                gallery: 'thumbnails',
                galleryActiveClass: 'active',
                cursor: "crosshair",
                responsive: true,
                easing: true,
                zoomWindowFadeIn: 500,
                zoomWindowFadeOut: 500,
                lensFadeIn: 500,
                lensFadeOut: 500
            });
            $(".open_qv").on("click", function (e) {
                var ez = $(this).siblings('img').data('elevateZoom');
                $.fancybox(ez.getGalleryList());
                e.preventDefault();
            });
        }
    }

    // -------chosen----------------------------------------------------

    $(".chosen-select").chosen({disable_search_threshold: 10});

    /* TOGGLE */
    function vereesa_dropdown() {
        $(document).on('click', '.header-control .close', function () {
            $(this).closest('.vereesa-dropdown').removeClass('open');
        });
        $(document).on('click', function (event) {
            var _target = $(event.target).closest('.vereesa-dropdown');
            var _allparent = $('.vereesa-dropdown');

            if (_target.length > 0) {
                _allparent.not(_target).removeClass('open');
                if (
                    $(event.target).is('[data-vereesa="vereesa-dropdown"]') ||
                    $(event.target).closest('[data-vereesa="vereesa-dropdown"]').length > 0
                ) {
                    _target.toggleClass('open');
                    return false;
                }
            } else {
                $('.vereesa-dropdown').removeClass('open');
            }
        });
    }

    function vereesa_mobile_block() {
        $(document).on('click', '.header-device-mobile .item.has-sub>a', function () {
            $(this).closest('.header-device-mobile').find('.item').removeClass('open');

            $(this).closest('.item').addClass('open');
            return false;
        })
        $(document).on('click', '.header-device-mobile .item .close', function () {
            $(this).closest('.item').removeClass('open');
            return false;
        })
        $(document).on('click', '*', function (event) {
            if (!$(event.target).closest(".header-device-mobile").length) {
                $(".header-device-mobile").find('.item').removeClass('open');
            }
        })
    }

    // --------------------------------BACK TO TOP-----------------------------
    $(window).on('scroll', function () {
        if ($(window).scrollTop() > 1000) {
            $('.backtotop').addClass('show');
        }
        else {
            $('.backtotop').removeClass('show');
        }
    });
    $(document).on('click', 'a.backtotop', function () {
        $('html, body').animate({scrollTop: 0}, 800);
    });
    //---------------------------Price filter----------------------
    $('.slider-range-price').each(function () {
        var min = $(this).data('min');
        var max = $(this).data('max');
        var unit = $(this).data('unit');
        var value_min = $(this).data('value-min');
        var value_max = $(this).data('value-max');
        var label_result = $(this).data('label-result');
        var t = $(this);
        $(this).slider({
            range: true,
            min: min,
            max: max,
            values: [value_min, value_max],
            slide: function (event, ui) {
                var result = ' <span>' + unit + ui.values[0] + ' </span>  <span> ' + unit + ui.values[1] + '</span>';
                // var result = label_result + " <span>" + unit + ui.values[0] + ' </span>  <span> ' + unit + ui.values[1] + '</span>';
                t.closest('.price-slider-wrapper').find('.price-slider-amount').html(result);
            }
        });
    });
//------------------------EQUAL ELEM----------------------------
    function better_equal_elems() {
        setTimeout(function () {
            $('.equal-container').each(function () {
                var $this = $(this);
                if ($this.find('.equal-element').length) {
                    $this.find('.equal-element').css({
                        'height': 'auto'
                    });
                    var elem_height = 0;
                    $this.find('.equal-element').each(function () {
                        var this_elem_h = $(this).height();
                        if (elem_height < this_elem_h) {
                            elem_height = this_elem_h;
                        }
                    });
                    $this.find('.equal-element').height(elem_height);
                }
            });
        }, 1000);
    }

    $(window).load(function () {
        better_equal_elems();
    });
    $(window).on("resize", function () {
        better_equal_elems();
    });

    // -----------------count down years months -------------------------------
    function vereesa_countdown() {
        if ($('.vereesa-countdown').length > 0) {
            var labels = ['Years', 'Months', 'Weeks', 'Days', 'Hrs', 'Mins', 'Secs'];
            var layout = '<span class="box-count day"><span class="number">{dnn}</span> <span class="text">Days</span></span><span class="box-count hrs"><span class="number">{hnn}</span> <span class="text">Hrs</span></span><span class="box-count min"><span class="number">{mnn}</span> <span class="text">Mins</span></span><span class="box-count secs"><span class="number">{snn}</span> <span class="text">Secs</span></span>';
            $('.vereesa-countdown').each(function () {
                var austDay = new Date($(this).data('y'), $(this).data('m') - 1, $(this).data('d'), $(this).data('h'), $(this).data('i'), $(this).data('s'));
                $(this).countdown({
                    until: austDay,
                    labels: labels,
                    layout: layout
                });
            });
        }
    };
    // --------------------------------------------------------
    $(window).scroll(function () {
        vereesa_custom_scrollbar();
    });
    $(window).resize(function () {
        quickview_popup();
        vereesa_remove_class_review();
        vereesa_details_thumd_zoom();
        vereesa_custom_scrollbar();
    });
    $(window).load(function () {
        quickview_popup();
        vereesa_mobile_block();
        vereesa_remove_class_review();
        vereesa_custom_scrollbar()
    });
    vereesa_dropdown();
    vereesa_remove_class_review();
    vereesa_details_thumd_zoom();
    vereesa_custom_scrollbar();
    vereesa_countdown();
    vereesa_init_menu_toggle();
    vereesa_custom_scrollbar_header_nav();
}); 