import {getDBConnection} from "../utils";
import {Types} from "mongoose";
import HTTPResponse from "../models/HTTPResponse";
import Coupon from "../models/Coupon";

export function verify(event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    console.log('Coupon code:', event.pathParameters.code);
    getDBConnection()
        .then(() => Coupon.findOne({code: event.pathParameters.code}))
        .then(coupon => {
            if(coupon === null) {
                callback(null, HTTPResponse({}))
            }

            const {code, type, discount} = coupon;
            callback(null, HTTPResponse({code, type, discount}))
        });
}