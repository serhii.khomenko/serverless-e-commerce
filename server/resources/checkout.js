import {getDBConnection} from "../utils";
import {Types} from "mongoose";
import HTTPResponse from "../models/HTTPResponse";
import Coupon from "../models/Coupon";

export function complete(event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    getDBConnection()
        .then(() => Coupon.find())
        .then(coupon => {
            callback(null, HTTPResponse({coupon}))
        });
}