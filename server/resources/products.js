import {getDBConnection} from '../utils'
import Product from '../models/Product'
import HTTPResponse from "../models/HTTPResponse";
import {Types} from 'mongoose'

const lookupReviewsPipe = {
    $lookup: {
        from: 'reviews',
        localField: '_id',
        foreignField: 'productId',
        as: 'reviews',
    }
};

const extractProductShortsInProject = {
    title: "$title",
    thumbnails: "$thumbnails",
    price: "$price",
    rate: {
        $avg: "$reviews.rate"
    },
    stars: {
        $size: "$reviews"
    }
};

export function list(event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    getDBConnection()
        .then(
            () => Product.aggregate([lookupReviewsPipe, {$project: extractProductShortsInProject}]).exec()
        )
        .then(products => callback(null, HTTPResponse({products})));
}

export function single(event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    getDBConnection()
        .then(() => Product.aggregate([
            lookupReviewsPipe,
            {
                $project: {
                    ...extractProductShortsInProject,
                    available: "$available",
                    attributes: "$attributes",
                    description: "$description"
                }
            }, {
                $match: {_id: Types.ObjectId(event.pathParameters.id)}
            }
        ]).exec())
        .then(productDetails => callback(null, HTTPResponse({productDetails})));
}