import mongoose from 'mongoose'
import {mongoConnectionString} from './config'

export const getDBConnection = () => {
    mongoose.set('useNewUrlParser', true);
    mongoose.set('useFindAndModify', false);
    mongoose.set('useCreateIndex', true);
    mongoose.set('useUnifiedTopology', true);

    return mongoose.connect(mongoConnectionString);
};
