import {Schema, model} from 'mongoose';

const ProductSchema = new Schema({
    title: String,
    description: String,
    thumbnails: [String],
    available: String,
    price: Number,
    attributes: [{
        label: String,
        type: String,
        options: [{
            hex: String,
            name: String
        }]
    }]
});

export default model('products', ProductSchema);