export default function (body = {sample: 'data'}, status = 200, headers = {}) {
    return {
        statusCode: status,
        headers: {
            ...headers,
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials": true
        },
        body: JSON.stringify(body, null, 2)
    }
}