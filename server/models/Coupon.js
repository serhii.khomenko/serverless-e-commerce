import {Schema, model} from 'mongoose';

const CouponSchema = new Schema({
    type: String,
    code: String,
    usages: Number,
    discount: Number
});

export default model('coupon', CouponSchema, 'coupon');